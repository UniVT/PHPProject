<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert(['name' => 'Първи']);
        DB::table('courses')->insert(['name' => 'Втори']);
        DB::table('courses')->insert(['name' => 'Трети']);
        DB::table('courses')->insert(['name' => 'Четвърти']);
    }
}
