<?php

use Illuminate\Database\Seeder;

class SpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('specialities')->insert(
            [
                'name' => 'Софтуерно Инженерство',
                'name_short' => 'СИ'
            ]
        );

        DB::table('specialities')->insert(
            [
                'name' => 'Информатика',
                'name_short' => 'И'
            ]
        );

        DB::table('specialities')->insert(
            [
                'name' => 'Математика',
                'name_short' => 'М'
            ]
        );

        DB::table('specialities')->insert(
            [
                'name' => 'Компютърни науки',
                'name_short' => 'КН'
            ]
        );
    }
}
