@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="row col-6 text-center">
            <form action="" method="get">
            <fieldset>
                <legend>Търсене</legend>
                <label for="studentName">Име: </label>
                <input id="studentName" name="studentName" type="text"><br />
                <label for="courseID">Курс: </label>
                <select id="courseID" name="courseID">
                    <option value="">--- Изберете ---</option>
                    @foreach ($c as $course)
                        <option value="{{ $course->id }}">
                            {{ $course->name }}
                        </option>
                    @endforeach
                </select><br />
                <label for="specialityID">Специалност: </label>
                <select id="specialityID" name="specialityID">
                    <option value="">--- Изберете ---</option>

                    @foreach ($specialities as $с)
                        <option value="{{ $с->id }}">
                            ({{ $с->name_short }}) {{ $с->name }}
                        </option>
                    @endforeach

                </select>&nbsp;&nbsp;
                <input type="submit" class="btn btn-primary" value="Търси">
            </fieldset>
            </form>
        </div>
        <br class="clearfix" />
        <div class="row col-12">
            <table class="table table-stripped table-responsive table-bordered">
                <thead class="thead-light justify-content-center">
                    <tr>
                        <td rowspan="2"></td>
                        <td colspan="2" rowspan="2"></td>
                        <td colspan="12"></td>
                    </tr>
                    <tr class="justify-content-center">
                        <td class="justify-content-center" colspan="3">Математика</td>
                        <td colspan="3">Информатика</td>
                        <td colspan="3">Физика</td>
                        <td colspan="3">Общо</td>
                    </tr>
                    <tr>
                        <td>№</td>
                        <td>Име, Фамилия</td>
                        <td>Курс</td>
                        <td>Лекции</td>
                        <td>Упражнения</td>
                        <td>Оценка</td>
                        <td>Лекции</td>
                        <td>Упражнения</td>
                        <td>Оценка</td>
                        <td>Лекции</td>
                        <td>Упражнения</td>
                        <td>Оценка</td>
                        <td>Лекции</td>
                        <td>Упражнения</td>
                        <td>Ср. успех</td>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
</div>
@endsection
