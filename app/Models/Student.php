<?php
namespace App\Models;

class Student
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'fnumber', 'email', 'course_id', 'speciality_id', 'education_form'
    ];
}
