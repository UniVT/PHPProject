<?php
namespace App\Models;


class Speciality
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'name_short',
    ];
}