<?php
namespace App\Models;

class Course
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
    
    /**
     * 
     * Get the students for course
     * 
     */
    public function students() 
    {
        return $this->hasMany('App\Models\Student');
    }
}
