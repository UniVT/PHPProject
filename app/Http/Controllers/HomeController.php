<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $studentName = $request->get('studentName', null);
        $courseID = $request->get('courseID');
        $specID = $request->get('specialityID');

        $courses = DB::table('courses')->get();
        $specialities = DB::table('specialities')->get();



        return view(
            'home',
                [
                    'c' => $courses,
                    'specialities' => $specialities
                ]
        );
    }
}
